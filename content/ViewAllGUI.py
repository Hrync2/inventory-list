from PyQt5.QtCore import QVariant, Qt, QSortFilterProxyModel
from PyQt5.QtGui import QStandardItem, QStandardItemModel, QIcon
from PyQt5.QtWidgets import QWidget, QTabWidget, QDialog, QVBoxLayout, QTableView, QLineEdit

from content.Order import Order
from content.User import User


class ViewAllGUI(QDialog):

    def __init__(self):
        super().__init__()

        # Tabs define #
        self.tabs = QTabWidget()
        self.tab_finished = QWidget()
        self.tab_paused = QWidget()
        self.tab_all = QWidget()

        # Tables define #
        self.table_finished = QTableView()
        self.table_paused = QTableView()
        self.table_all = QTableView()

        # Models define #
        self.table_finished_model = QStandardItemModel()
        self.table_paused_model = QStandardItemModel()
        self.table_all_model = QStandardItemModel()

        # filter proxy model
        self.filter_proxy_model_all = QSortFilterProxyModel()
        self.filter_proxy_model_finished = QSortFilterProxyModel()

        self.init_ui()

    def init_ui(self):
        self.tabs.addTab(self.tab_finished, "Zakończone")
        self.tabs.addTab(self.tab_paused, "Wstrzymane")
        self.tabs.addTab(self.tab_all, "Wszystkie")

        self.tab_all_ui()
        self.tab_paused_ui()
        self.tab_finished_ui()

        self.setLayout(QVBoxLayout())
        self.layout().addWidget(self.tabs)

        self.setFixedSize(640, 800)
        self.setWindowTitle('Widok wszystkich pozycji')
        self.setWindowIcon(QIcon('content\\warehouse.png'))
        self.setWindowFlags(Qt.WindowMinimizeButtonHint | Qt.WindowCloseButtonHint)
        self.setModal(True)

    def tab_all_ui(self):
        layout = QVBoxLayout(self.tab_all)
        # Filter proxy model
        self.filter_proxy_model_all.setSourceModel(self.table_all_model)
        self.filter_proxy_model_all.setFilterKeyColumn(0)
        # Line edit for filtering
        line_edit = QLineEdit()
        line_edit.textChanged.connect(self.filter_proxy_model_all.setFilterRegExp)
        line_edit.setPlaceholderText("Filtruj: Nr. zlecenia")
        # Table
        self.table_all = QTableView()
        self.table_all.setModel(self.filter_proxy_model_all)
        self.table_all.setSortingEnabled(True)
        layout.addWidget(line_edit)
        layout.addWidget(self.table_all)

    def tab_paused_ui(self):
        layout = QVBoxLayout()
        self.table_paused = QTableView()
        self.table_paused.setModel(self.table_paused_model)
        self.table_paused.setSortingEnabled(True)
        layout.addWidget(self.table_paused)
        self.tab_paused.setLayout(layout)

    def tab_finished_ui(self):
        layout = QVBoxLayout(self.tab_finished)
        # Filter proxy model
        self.filter_proxy_model_finished.setSourceModel(self.table_finished_model)
        self.filter_proxy_model_finished.setFilterKeyColumn(0)
        # Line edit for filtering
        line_edit = QLineEdit()
        line_edit.textChanged.connect(self.filter_proxy_model_finished.setFilterRegExp)
        line_edit.setPlaceholderText("Filtruj: Nr. zlecenia")
        # Table
        self.table_finished = QTableView()
        self.table_finished.setModel(self.filter_proxy_model_finished)
        self.table_finished.setSortingEnabled(True)
        layout.addWidget(line_edit)
        layout.addWidget(self.table_finished)

    def show_(self):
        self.model_for_all_table()
        self.model_for_paused_table()
        self.model_for_finished_table()
        self.show()

    def model_for_all_table(self):
        self.table_all_model.clear()

        # Set column names in all tables
        self.table_all_model.setHorizontalHeaderLabels(
            ["Nr. zlecenia", "Typ", "Status", "Data akcji", "Użytkownik", "Czas[h]"])

        orders = Order.get_orders_list()
        orders = ViewAllGUI.organize_names_in_order_fields_globally(orders)

        for o in orders:
            # Set items
            order = o.get_order_data()
            items_in_all_tab = []
            for item in order:
                qsi = QStandardItem()
                qsi.setData(QVariant(item), Qt.DisplayRole)
                items_in_all_tab.append(qsi)
            # Add formatted data to row in tables
            if len(items_in_all_tab) > 0:
                self.table_all_model.appendRow(items_in_all_tab)
        self.table_all.resizeColumnsToContents()
        self.table_all.sortByColumn(3, Qt.DescendingOrder)

    def model_for_paused_table(self):
        self.table_paused_model.clear()

        # Set column names in all tables
        self.table_paused_model.setHorizontalHeaderLabels(
            ["Nr. zlecenia", "Typ", "Status", "Data wstrzymania", "Użytkownik", "Czas[h]"])

        orders = Order.get_paused_unfinished_orders_list()
        orders = ViewAllGUI.organize_names_in_order_fields_globally(orders)

        for o in orders:
            order = o.get_order_data()
            items_in_paused_tab = []
            for item in order:
                qsi = QStandardItem()
                qsi.setData(QVariant(item), Qt.DisplayRole)
                items_in_paused_tab.append(qsi)
            if len(items_in_paused_tab) > 0:
                self.table_paused_model.appendRow(items_in_paused_tab)
        self.table_paused.resizeColumnsToContents()
        self.table_paused.sortByColumn(3, Qt.DescendingOrder)

    def model_for_finished_table(self):
        self.table_finished_model.clear()

        # Set column names in all tables
        self.table_finished_model.setHorizontalHeaderLabels(
            ["Nr. zlecenia", "Typ", "Status", "Data zakończenia", "Czas[h]"])

        orders = Order.get_finished_orders_list()
        orders = ViewAllGUI.organize_names_in_order_fields_globally(orders)

        for o in orders:
            order = o.get_order_data()
            order.remove(order[4])
            items_in_finished_tab = []
            for item in order:
                qsi = QStandardItem()
                qsi.setData(QVariant(item), Qt.DisplayRole)
                items_in_finished_tab.append(qsi)
            if len(items_in_finished_tab) > 0:
                self.table_finished_model.appendRow(items_in_finished_tab)
        self.table_finished.resizeColumnsToContents()
        self.table_finished.sortByColumn(3, Qt.DescendingOrder)

    @staticmethod
    def organize_names_in_order_fields_globally(orders):
        for order in orders:
            if order.order_type == 'A':
                order.order_type = 'Akcesoria'
            else:
                order.order_type = 'Profile'
            if order.status == '1':
                order.status = 'Rozpoczęte'
            elif order.status == '2':
                order.status = 'Wstrzymane'
            else:
                order.status = 'Zakończone'
            # Set user name instead of number
            for user in User.get_users_list():
                if user.user_no == order.user_no:
                    order.user_no = user.first_name + ' ' + user.surname
            # Set duration in hours
            if order.duration != '0':
                order.duration = round(int(order.duration) / 3600, 1)
        return orders
