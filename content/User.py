import re


class User:
    def __init__(self):
        self.user_no = ''
        self.first_name = ''
        self.surname = ''

    def set_user_data(self, user_no, first_name, surname):
        self.user_no = user_no
        self.first_name = first_name
        self.surname = surname

    @staticmethod
    def get_users_list():
        users = []
        with open("content\\wmusers.csv", "r", encoding='utf-8') as user_file:
            for row in user_file:
                row = re.split("[;]", row)
                user = User()
                user.set_user_data(
                    row[0], row[1], row[2])
                users.append(user)
        return users
