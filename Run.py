import sys

from PyQt5.QtGui import QFont
from PyQt5.QtWidgets import QApplication

from content.wmappGUI import WMGUI

if __name__ == '__main__':
    app = QApplication(sys.argv)
    app.setStyle("Fusion")
    app.setFont(QFont("Arial", 9))
    ex = WMGUI()
    sys.exit(app.exec_())
