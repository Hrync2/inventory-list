# -*- coding: utf-8 -*-
import re
from datetime import datetime

from PyQt5.QtCore import Qt
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QLineEdit, QPushButton, QLabel, QListWidget, QDialog

from content.Order import Order
from content.User import User
from content.ViewAllGUI import ViewAllGUI
from content.AdminPanelGUI import AdminPanelGUI


class WMGUI(QDialog):

    def __init__(self):
        super().__init__()
        self.view_all = ViewAllGUI()
        self.admin_panel = AdminPanelGUI()
        self.user_field = QLineEdit(self)
        self.order_field = QLineEdit(self)
        self.start_button = QPushButton('START', self)
        self.pause_button = QPushButton('PAUZA', self)
        self.finish_button = QPushButton('FINISH', self)
        self.view_all_button = QPushButton("Pokaż wszystkie", self)
        self.admin_panel_button = QPushButton(" Panel Administratora", self)
        self.current_orders_table = QListWidget(self)
        self.current_orders = Order.get_started_unfinished_orders_list()
        self.init_ui()

    def init_ui(self):
        QLabel("Użytkownik", self).setGeometry(40, 20, 100, 20)
        QLabel("Zlecenie", self).setGeometry(40, 80, 100, 20)
        QLabel("Aktualnie uruchomione", self).setGeometry(40, 380, 200, 20)
        self.user_field.setGeometry(40, 40, 560, 22)
        self.user_field.setMaxLength(13)
        self.user_field.textChanged.connect(self.user_field_check)

        self.order_field.setGeometry(40, 100, 560, 22)
        self.order_field.setMaxLength(10)
        self.order_field.setDisabled(True)
        self.order_field.textChanged.connect(self.order_field_check)

        self.start_button.setGeometry(40, 200, 100, 100)
        self.start_button.clicked.connect(lambda: self.mode_button_pushed('1'))
        self.pause_button.setGeometry(270, 200, 100, 100)
        self.pause_button.clicked.connect(lambda: self.mode_button_pushed('2'))
        self.finish_button.setGeometry(500, 200, 100, 100)
        self.finish_button.clicked.connect(lambda: self.mode_button_pushed('3'))
        self.disable_buttons(True, True, True)
        self.view_all_button.setGeometry(480, 720, 120, 30)
        self.view_all_button.clicked.connect(self.view_all_button_pushed)
        self.admin_panel_button.setGeometry(40, 720, 150, 30)
        self.admin_panel_button.setIcon(QIcon('content\\admin_ico.png'))
        self.admin_panel_button.clicked.connect(self.admin_button_pushed)

        self.current_orders_table.setGeometry(40, 400, 560, 300)
        self.update_current_orders_table()

        self.setFixedSize(640, 800)
        self.setWindowTitle('Wykaz Magazynowy')
        self.setWindowIcon(QIcon('content\\warehouse.png'))
        self.setWindowFlags(Qt.WindowMinimizeButtonHint | Qt.WindowCloseButtonHint)
        self.show()

    def user_field_check(self):
        if len(self.user_field.text()) == 13:
            for user in User.get_users_list():
                if user.user_no == (self.user_field.text()):
                    self.order_field.setDisabled(False)
                    self.order_field.setFocus()
        else:
            self.order_field.clear()
            self.order_field.setDisabled(True)

    def order_field_check(self):
        if len(self.order_field.text()) >= 5:
            order_data = re.split('[_ ]', self.order_field.text())
            if len(order_data) >= 2:
                for item in self.current_orders:
                    if item.order_no + item.order_type == order_data[0] + order_data[1] \
                            and self.user_field.text() == item.user_no:
                        self.disable_buttons(True, False, False)
                        break
                    else:
                        self.disable_buttons(False, True, True)
        else:
            self.disable_buttons()

    def mode_button_pushed(self, mode):
        order = Order()
        order_data = re.split('[_ ]', self.order_field.text())  # regex underline and whitespace
        if len(order_data) >= 2:
            if mode == '1':  # START button pressed
                order.set_order_data(order_data[0], order_data[1], mode, self.get_current_time(),
                                     self.user_field.text(), 0)
                order.write_order_to_file()
                self.current_orders.append(order)
                self.update_current_orders_table()
                self.order_field.clear()
                self.user_field.clear()
                self.user_field.setFocus()
            elif mode == '2' or mode == '3':  # PAUSE or FINISH button pressed
                for item in self.current_orders:
                    if item.order_no == order_data[0] and item.order_type == order_data[1]:
                        duration = self.get_current_time() - datetime.strptime(str(item.date_time), "%Y-%m-%d %H:%M:%S")
                        order.set_order_data(order_data[0], order_data[1], mode, self.get_current_time(),
                                             self.user_field.text(), int(duration.total_seconds()))
                        order.write_order_to_file()
                        self.current_orders.remove(item)
                        self.update_current_orders_table()
                        self.order_field.clear()
                        self.user_field.clear()
                        self.user_field.setFocus()
                        break

    def update_current_orders_table(self):
        self.current_orders_table.clear()
        for item in self.current_orders:
            self.current_orders_table.addItem(item.order_no + item.order_type)

    def view_all_button_pushed(self):
        self.view_all.show_()

    def admin_button_pushed(self):
        self.admin_panel.show()

    def disable_buttons(self, start_b=True, pause_b=True, finish_b=True):
        self.start_button.setDisabled(start_b)
        self.finish_button.setDisabled(pause_b)
        self.pause_button.setDisabled(finish_b)

    @staticmethod
    def get_current_time():
        return datetime.now().replace(microsecond=0)
