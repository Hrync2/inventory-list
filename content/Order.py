import re


class Order:
    def __init__(self):
        self.order_no = ''
        self.order_type = ''
        self.status = ''
        self.date_time = None
        self.user_no = ''
        self.duration = 0

    def write_order_to_file(self):
        with open('content\\order_list.csv', 'a', encoding='utf-8') as order_file:
            order_file.write(self.order_no + ";" + self.order_type + ";" + self.status + ";" +
                             str(self.date_time) + ";" + self.user_no + ";" + str(self.duration) + "\n")

    def set_order_data(self, order_no, order_type, status, currene_time, user, duration):
        self.order_no = order_no
        self.order_type = order_type
        self.status = status
        self.date_time = currene_time
        self.user_no = user
        self.duration = duration

    def get_order_data(self):
        return [self.order_no, self.order_type, self.status, self.date_time, self.user_no, self.duration]

    @staticmethod
    def get_orders_list():
        orders = []
        with open("content\\order_list.csv", "r", encoding='utf-8') as order_file:
            for row in order_file:
                row = re.split("[;]", row)
                order = Order()
                order.set_order_data(
                    row[0], row[1], row[2], row[3], row[4], int(row[5].rstrip()))
                orders.append(order)
        return orders

    @staticmethod
    def get_started_unfinished_orders_list():
        started = []
        for order in Order.get_orders_list():
            if order.status == '1':
                started.append(order)
            else:
                for s in started:
                    if s.order_no == order.order_no and s.order_type == order.order_type and s.user_no == order.user_no:
                        started.remove(s)
                        break
        return started

    @staticmethod
    def get_paused_unfinished_orders_list():
        paused = []
        paused_cpy = []
        finished = []
        for order in Order.get_orders_list():
            if order.status == '2':
                paused.append(order)
            elif order.status == '3':
                finished.append(order)
        for p in paused:
            paused_cpy.append(p)
        for p in paused_cpy:
            for f in finished:
                if p.order_no == f.order_no and p.order_type == f.order_type:
                    paused.remove(p)
        return paused

    @staticmethod
    def get_finished_orders_list():
        paused = []
        finished = []
        for order in Order.get_orders_list():
            if order.status == '2':
                paused.append(order)
            elif order.status == '3':
                finished.append(order)
        for f in finished:
            for p in paused:
                if f.order_no == p.order_no and f.order_type == p.order_type:
                    f.duration = f.duration + p.duration
        return finished
