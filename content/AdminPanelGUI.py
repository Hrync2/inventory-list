from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QDialog, QPushButton, QVBoxLayout


class AdminPanelGUI(QDialog):

    def __init__(self):
        super().__init__()

        self.users_button = QPushButton("Edycja użytkowników", self)
        self.orders_button = QPushButton("Edycja zlecenia", self)

        self.init_ui()

    def init_ui(self):

        self.setLayout(QVBoxLayout())
        self.layout().addWidget(self.users_button)
        self.layout().addWidget(self.orders_button)

        self.setWindowTitle('Panel Administratora')
        self.setWindowIcon(QIcon('content\\admin_ico.png'))
        self.setFixedSize(640, 800)
        self.setModal(True)
